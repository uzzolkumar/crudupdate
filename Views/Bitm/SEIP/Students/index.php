<?php

include_once("../../../../vendor/autoload.php");
use App\Bitm\SEIP\Students\Students;

?>
<html>
<head>
    <title>View informaiton</title>
</head>

<body>
<table border="1">
    <thead>List of Students</thead>
    <tr>
        <td>ID</td>
        <td>Title</td>
        <td>Action</td>
    </tr>

    <?php

    $obj = new Students();

    $data = $obj->index();

    foreach ($data as $key => $value) {
        ?>
        <tr>
            <td><?php echo $key + 1; ?> </td>
            <td><?php echo $value['title']; ?></td>
            <td>
                <a href="show.php?id=<?php echo $value['id'];?>">View</a> |
                <a href="edit.php?id=<?php echo $value['id'];?>">Edit</a> |
                <a href="delete.php?id=<?php echo $value['id'];?>">Delete</a></td>
        </tr>
    <?php } ?>
</table>
</body

</html>


