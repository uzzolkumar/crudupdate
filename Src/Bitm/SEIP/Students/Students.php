<?php
namespace App\Bitm\SEIP\Students;

use pdo;


class Students
{
    public $name;
    public $id;


    public function setData($data = '')
    {
    if(array_key_exists('title',$data))
    {
        $this->name = $data['title'];
    }
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }

        return $this;
    }

    public function index()
    {
        try {
            $pdo = new pdo('mysql:host=localhost;dbname=php38', 'root', '');
            $query = "SELECT * FROM `student`";

            $stmt = $pdo->prepare($query);

            $stmt->execute();

            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }

    public function Store()
    {
        try {
            $pdo = new pdo('mysql:host=localhost;dbname=php38', 'root', '');
            $query = "INSERT INTO `student` (`id`, `title`) VALUES (:a,:b)";

            $stmt = $pdo->prepare($query);

            $stmt->execute(
                array(
                    ':a' => 'null',
                    ':b' => $this->name

                )
            );
            if ($stmt) {
                session_start();
                header("location:create.php");
                $_SESSION['message'] = "Data Submitted successfully";
            }

        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }

    }
    public function show()
    {
        try {
            $pdo = new pdo('mysql:host=localhost;dbname=php38', 'root', '');
            $query = "SELECT * FROM `student` WHERE id=$this->id";

            $stmt = $pdo->prepare($query);

            $stmt->execute();

            $data = $stmt->fetch();
            return $data;
        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
    }
//    public function update()
//    {
//        try {
//            $pdo = new pdo('mysql:host=localhost;dbname=php38', 'root', '');
//            $query = "UPDATE `student` SET `title` = 'Bangladesh' WHERE `student`.`id` = 4;";
//
//            $stmt = $pdo->prepare($query);
//
//            $stmt->execute();
//
//            $data = $stmt->fetch();
//            return $data;
//        } catch (PDOException $e) {
//            echo "Error" . $e->getMessage();
//        }
//    }

}

